﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulasiTes.Models
{
    public class dataRumah
    {

        public int ID { get; set; }
        public string Cityname { get; set; }
        public int Latitude { get; set; }
        public int Longitude { get; set; }
        public string Keterangan { get; set; } 
        public string Tipe { get; set; }
        public string Alamat { get; set; }
    }
}
