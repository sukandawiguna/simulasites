﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Protocols;
using SimulasiTes.Data;
using SimulasiTes.Models;

namespace SimulasiTes.Controllers
{
    public class DataRumahController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DataRumahController(ApplicationDbContext context)
        {
            _context = context;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Location()
        {
            string markers = "[";
            using (SqlConnection con = new SqlConnection())
            {
                SqlCommand cmd = new SqlCommand("spGetMap", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    markers += "{";
                    markers += string.Format("'title': '{0}',", sdr["CityName"]);
                    markers += string.Format("'lat': '{0}',", sdr["Latitude"]);
                    markers += string.Format("'lng': '{0}',", sdr["Longitude"]);
                    markers += string.Format("'description': '{0}'", sdr["Keterangan"]);
                    markers += "},";
                }
            }
            markers += "];";
            ViewBag.Markers = markers;
            return View();
        }

        [HttpPost]
        public ActionResult Location(dataRumah rumah)
        {
            if (ModelState.IsValid)
            {
                using (SqlConnection con = new SqlConnection())
                {
                    SqlCommand cmd = new SqlCommand("spAddNewLocation", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    cmd.Parameters.AddWithValue("@CityName", rumah.Cityname);
                    cmd.Parameters.AddWithValue("@Latitude", rumah.Latitude);
                    cmd.Parameters.AddWithValue("@Longitude", rumah.Longitude);
                    cmd.Parameters.AddWithValue("@Description", rumah.Keterangan);
                    cmd.ExecuteNonQuery();
                }
            }
            else
            {

            }
            return RedirectToAction("Location");
        }
    }
}